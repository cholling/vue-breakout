import Vue from 'vue'
import Vuex from 'vuex'
import * as li from 'line-intersection'

Vue.use(Vuex)

const levelOne = [
  [1, 1, 1, 2, 2, 1, 1, 1],
  [1, 1, 2, 3, 3, 2, 1, 1],
  [1, 2, 3, 1, 1, 3, 2, 1],
  [2, 3, 1, 1, 1, 1, 3, 2],
  [2, 3, 1, 1, 1, 1, 3, 2],
  [2, 3, 1, 1, 1, 1, 3, 2],
  [2, 3, 1, 1, 1, 1, 3, 2],
  [1, 2, 3, 1, 1, 3, 2, 1],
  [1, 1, 2, 3, 3, 2, 1, 1],
  [1, 1, 1, 2, 2, 1, 1, 1]
]

function startingBarWidth (state) {
  return state.vboxWidth / 9
}

function barWidth (state) {
  return startingBarWidth(state) * state.barWidthMultiplier
}

function barHeight (state) {
  return (state.vboxHeight / 27)
}

function getRow (state, y) {
  return Math.floor(y / barHeight(state))
}

function getColumn (state, x) {
  return Math.floor((x - (startingBarWidth(state) / 2)) / startingBarWidth(state))
}

function trapBall (state) {
  state.ballx = state.barx + barWidth(state) / 2
  state.bally = state.vboxHeight - barHeight(state)
  state.ballTrapped = true
}

function getBlockCorners(state, row, column) {
  let leftSide = (startingBarWidth(state) / 2) + column * startingBarWidth(state)
  let rightSide = leftSide + startingBarWidth(state)
  let top = row * barHeight(state)
  let bottom = top + barHeight(state)
  return {
    topLeft: {
      x: leftSide,
      y: top
    },
    topRight: {
      x: rightSide,
      y: top
    },
    bottomLeft: {
      x: leftSide,
      y: bottom
    },
    bottomRight: {
      x: rightSide,
      y: bottom
    }
  }
}

export default new Vuex.Store({
  state: {
    barx: 0,
    barWidthMultiplier: 1,
    vboxWidth: 270,
    vboxHeight: 360,
    ballx: 10,
    bally: 10,
    ballxSpeed: 1,
    ballySpeed: 1,
    ballTrapped: true,
    blockState: levelOne
  },
  getters: {
    barWidth: barWidth,
    barHeight: barHeight
  },
  mutations: {
    setx (state, x) {
      state.barx = x
      if (state.ballTrapped) {
        state.ballx = x + barWidth(state) / 2
      }
    },
    moveBall (state) {
      if (!state.ballTrapped) {
        let newx = state.ballx + state.ballxSpeed
        let newy = state.bally + state.ballySpeed
        if (newx < 0 || newx > state.vboxWidth) {
          newx = state.ballx
          state.ballxSpeed = -1 * state.ballxSpeed
        }
        if (newy < 0 ||
          (newx >= state.barx &&
            newx <= (state.barx + barWidth(state)) &&
            newy > (state.vboxHeight - barHeight(state))
          )
        ) {
          newy = state.bally
          state.ballySpeed = -1 * state.ballySpeed
        }
        let column = getColumn(state, newx)
        let row = getRow(state, newy)
        if (row >= 0 &&
          row < state.blockState.length &&
          column >= 0 &&
          column < state.blockState[0].length &&
          state.blockState[row][column] !== 0
        ) {
          state.blockState[row].splice(column, 1, state.blockState[row][column] - 1)
          let currentPoint = {x: state.ballx, y: state.bally}
          let newPoint = {x: newx, y: newy}
          let corners = getBlockCorners(state, row, column)
          let xintersection = null
          let yintersection = null
          if (state.ballySpeed < 0) {
            yintersection = li.findSegmentIntersection([
              currentPoint,
              newPoint,
              corners.bottomLeft,
              corners.bottomRight
            ])
          } else {
            yintersection = li.findSegmentIntersection([
              currentPoint,
              newPoint,
              corners.topLeft,
              corners.topRight
            ])
          }
          if (state.ballxSpeed < 0) {
            xintersection = li.findSegmentIntersection([
              currentPoint,
              newPoint,
              corners.topRight,
              corners.bottomRight
            ])
          } else {
            xintersection = li.findSegmentIntersection([
              currentPoint,
              newPoint,
              corners.topLeft,
              corners.topRight
            ])
          }
          if (yintersection) {
            state.ballySpeed = -1 * state.ballySpeed
            newy = state.bally
          }
          if (xintersection) {
            state.ballxSpeed = -1 * state.ballxSpeed
            newx = state.ballx
          }
        }
        state.ballx = newx
        state.bally = newy
        if (newy > state.vboxHeight) {
          trapBall(state)
        }
      }
    },
    releaseBall (state) {
      if (state.ballTrapped) {
        state.ballxSpeed = 1
        state.ballySpeed = -1
        state.ballTrapped = false
      }
    },
    trapBall: trapBall
  }
})
